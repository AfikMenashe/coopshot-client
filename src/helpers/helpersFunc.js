import { setStatusBarNetworkActivityIndicatorVisible } from "expo-status-bar";
import axios from "../api/albums";

export function updatePartners(albumId, usersArr) {
  axios
    .put("/album/addpartners", { albumid: albumId, users: usersArr })
    .then((res) => {
      console.log(
        "Updating the partners of ht ealbum -> respone : " + JSON.stringify(res)
      );
    })
    .catch((e) =>
      console.log("There is a problem with updating partners: \n" + e)
    )
    .done();
}

export function updatePartner(albumId, user) {
  axios
    .put("/album/addpartner", { albumid: albumId, user: user })
    .then((res) => {
      console.log("Updating the partners of the album -> respone : ");
    })
    .catch((e) =>
      console.log("There is a problem with updating partners: \n" + e)
    )
    .done();
}

export function removePartner(albumId, user) {
  axios
    .put("/album/removePartner", { albumID: albumId, partnerID: user })
    .then((res) => {
      console.log("Delete partner of the album -> respone : ");
    })
    .catch((e) =>
      console.log("There is a problem with removing partners: \n" + e)
    )
    .done();
}
