/** @format */

// import { AsyncStorage } from '@react-native-community/async-storage'; // has to be done by eject - not with expo
import { AsyncStorage } from "react-native";
import createDataContext from "./createDataContext";
import albumsApi from "../api/albums";
import { navigate } from "../navigationRef";
import shorthash from "shorthash";
import * as FileSystem from "expo-file-system";

const authReducer = (state, action) => {
  switch (action.type) {
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "signup":
      return { errorMessage: "", token: action.payload };
    case "signin":
      return {
        errorMessage: "",
        token: action.payload[0],
        userID: action.payload[1],
      };
    case "clear_error_message":
      return { ...state, errorMessage: "" };
    case "signout":
      return { token: null, errorMessage: "" };
    default:
      return state;
  }
};

const tryLocalSignin = (dispatch) => async () => {
  var token;
  try {
    token = await AsyncStorage.getItem("token");
  } catch (err) {
    console.log("My trylocalsignin error : " + err);
    token = null;
  }
  if (token) {
    dispatch({ type: "signin", payload: token });
    navigate("Home");
  } else {
    navigate("SignIn");
  }
};

const signup =
  (dispatch) =>
  async ({ email, password, phone, name }) => {
    try {
      //Make an API request
      const response = await albumsApi.post("/signup", {
        email,
        password,
        phone,
        name,
      });
      await AsyncStorage.setItem("token", response.data.token);
      await AsyncStorage.removeItem("retUserId");
      await AsyncStorage.setItem("retUserId", response.data.userID);

      const imageBaseUrl =
        "https://res.cloudinary.com/yarivmenachem/image/upload/v1621423371/profle_zv2uxy.png";
      const image = await checkImageInCache(imageBaseUrl);

      const user = {
        email,
        password,
        phone,
        name,
        image,
      };

      await AsyncStorage.setItem(
        response.data.userID + " user details",
        JSON.stringify(user),
        () => console.log("Saved the user details")
      );

      dispatch({ type: "signup", payload: response.data.token });
      navigate("Home");
    } catch (err) {
      dispatch({
        type: "add_error",
        payload: "Something went wrong with sign up",
      });
    }
  };

const clearErrorMessage = (dispatch) => () => {
  dispatch({ type: "clear_error_message" });
};

const signin = (dispatch) => {
  return async ({ email, password }) => {
    try {
      const response = await albumsApi.post("/signin", { email, password });
      await AsyncStorage.setItem("token", response.data.token);
      await AsyncStorage.setItem("retUserId", response.data.userID);
      // Handle success by updating state
      const phone = response.data.phone;
      const name = response.data.name;
      const imageBaseUrl = response.data.image;
      const image = await checkImageInCache(imageBaseUrl);

      //create the user object to store
      const user = {
        email,
        password,
        phone,
        name,
        image,
      };

      //update the value
      await AsyncStorage.setItem(
        response.data.userID + " user details",
        JSON.stringify(user),
        () => console.log("Saved the user details")
      );
      dispatch({
        type: "signin",
        payload: [response.data.token, response.data.userID],
      });
      navigate("Home");
    } catch (err) {
      // Handle failure by showing error message (somehow)
      dispatch({
        type: "add_error",
        payload: "Something went wrong with sign in",
      });
    }
  };
};

const signout = (dispatch) => {
  return async () => {
    await AsyncStorage.removeItem("token");
    await AsyncStorage.removeItem("retUserId");
    dispatch({ type: "signout" });
    navigate("ResolveAuthScreen");
  };
};

const checkImageInCache = async (element) => {
  const name = shorthash.unique(element);
  const path = `${FileSystem.cacheDirectory}${name}`;
  const image = await FileSystem.getInfoAsync(path);
  if (image.exists) {
    return image.uri;
  } else {
    const newImage = await FileSystem.downloadAsync(element, path);
    return newImage.uri;
  }
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signin, signout, signup, clearErrorMessage, tryLocalSignin },
  { token: null, errorMessage: "", userID: "" }
);
