import createDataContext from "./createDataContext";
import axios from "../api/albums";
import shorthash from "shorthash";
import * as FileSystem from "expo-file-system";
import { AsyncStorage } from "react-native";

const albumReducer = (state, action) => {
  switch (action.type) {
    case "create_album": {
      return [...state, action.payload];
    }
    case "delete_album":
      return state.filter((album) => album.id !== action.payload);

    case "edit_album":
      return state.map((album) => {
        if (album._id === action.payload.id) {
          album.description = action.payload.description;
          album.name = action.payload.name;
          return album;
        } else return album;
      });

    case "delete_photo":
      return state.map((album, index) => {
        if (album._id === action.payload.id) {
          album.photos.filter(
            (image) => image._id !== action.payload.photoDeleteID
          );
          return album;
        } else return album;
      });

    case "get_albums":
      return action.payload;

    case "set_all_state":
      return action.payload.state;

    case "get_album": {
      return state.map((album) => {
        if (album._id === action.payload.albumId) {
          album.photos = action.payload.photos;
          album.profileImages = action.payload.profileImages;
          return album;
        } else {
          return album;
        }
      });
    }
    case "set_new_state": {
      return action.payload.data;
    }
    default:
      return state;
  }
};

const setNewState = (dispatch) => {
  return (data) => {
    dispatch({ type: "set_new_state", payload: { data } });
  };
};

const setAllStateAlbum = (dispatch) => {
  return async () => {
    var x;
    try {
      const userID = await AsyncStorage.getItem("retUserId");
      x = await AsyncStorage.getItem(userID + " Albums State Coopshoot");
    } catch (err) {
      x = null;
    }
    if (x) {
      dispatch({
        type: "set_all_state",
        payload: { state: JSON.parse(x) },
      });
    } else {
      dispatch({
        type: "set_all_state",
        payload: { state: [] },
      });
    }
  };
};

const getAlbum = (dispatch) => {
  return async (albumID, userID) => {
    // GET request for the album ditails -> search the album using the ID
    const response = await axios.get("/album/single", {
      params: {
        albumId: albumID,
      },
    });
    // GET request for the album's photos -> search in Photo collection the photos that connected to album using the ID
    // const photosResponse = await axios.get('photos', {
    //   params: { albumId: albumID, userID: userID },
    // });

    const photosResponse = response.data.photos;

    var cachedImages = [];

    for (let i = 0; i < photosResponse.length; i++) {
      const cacheImage = await checkImageInCache(photosResponse[i]);
      cachedImages.push(cacheImage);
    }

    var profileImages = [];

    for (partner of response.data.partners) {
      profileImages.push({ _id: partner._id, uri: partner.imageUrl });
    }

    dispatch({
      type: "get_album",
      payload: {
        albumId: response.data._id,
        photos: cachedImages,
        profileImages,
      },
    });
  };
};

const checkImageInCache = async (element) => {
  const uri = element.url;
  const name = shorthash.unique(uri);
  const path = `${FileSystem.cacheDirectory}${name}`;
  const image = await FileSystem.getInfoAsync(path);
  const resultImage = element;
  if (image.exists) {
    resultImage.url = image.uri;
  } else {
    const newImage = await FileSystem.downloadAsync(uri, path);
    resultImage.url = newImage.uri;
  }
  return resultImage;
};

const getAlbums = (dispatch) => {
  return async () => {
    //response.data === [{Album1},{Album2},{...}]
    const response = await axios.get("/album/all").then((albums) => {
      const albums2 = albums.data.map((item) => {
        return {
          _id: item._id,
          name: item.name.split("~")[1],
          photos: item.photos,
          description: item.description,
          photosQuantity: item.photosQuantity,
        };
      });
      return albums2;
    });

    for (let i = 0; i < response.length; i++) {
      for (let j = 0; j < response[i].photos.length; j++) {
        const cacheImage = await checkImageInCache(response[i].photos[j]);
        response[i].photos[j] = cacheImage;
      }
    }
    dispatch({ type: "get_albums", payload: response });
  };
};

const createNewAlbum = (dispatch) => {
  return async (albumName, description, callback) => {
    // await jsonServer.post('/albums',{name : albumName,description : description});
    const response = await axios.post(
      "/api/createalbum",
      {
        albumName: albumName,
        description: description,
      },
      {
        "Content-Type": "application/json",
        // 'Authorization': 'Bearer '+token
      }
    );
    response.data.name = response.data.name.split("~")[1];
    dispatch({ type: "create_album", payload: response.data });
    if (callback) {
      callback(response.data._id);
    }
  };
};

const deleteAlbum = (dispatch) => {
  return async (id, callback) => {
    dispatch({ type: "delete_album", payload: id });
    callback();
  };
};

const deletePhotoAlbum = (dispatch) => {
  return async (id, photoDeleteID, callback) => {
    dispatch({
      type: "delete_photo",
      payload: { id: id, photoDeleteID: photoDeleteID },
    });
    callback();
  };
};

const editAlbum = (dispatch) => {
  return async (id, albumName, description, callback) => {
    axios.put("/album/edit", {
      albumId: id,
      albumName: albumName,
      albumDesc: description,
    });
    dispatch({
      type: "edit_album",
      payload: { id: id, name: albumName, description: description },
    });
    callback();
  };
};

export const { Context, Provider } = createDataContext(
  albumReducer,
  {
    createNewAlbum,
    getAlbums,
    deleteAlbum,
    editAlbum,
    getAlbum,
    deletePhotoAlbum,
    setAllStateAlbum,
    setNewState,
  },
  []
);
