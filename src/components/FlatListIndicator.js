/** @format */

import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Entypo } from "@expo/vector-icons";
import { updatePartner, removePartner } from "../helpers/helpersFunc";

const FlatListIndicator = (props) => {
  const iconName = props.isPartner ? "trash" : "add-user";
  const [icon, setIcon] = useState(iconName);
  const toggleIcon = () => {
    if (icon === "trash") {
      removePartner(props.albumIdSent, props.userIdSent);
      setIcon("add-user");
    } else {
      updatePartner(props.albumIdSent, props.userIdSent);
      setIcon("trash");
    }
  };
  return (
    <View>
      <TouchableOpacity onPress={toggleIcon}>
        <Entypo name={icon} size={24} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({});

export default FlatListIndicator;
