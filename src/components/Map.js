import React from "react";
import { StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import MapView, { Marker } from "react-native-maps";

const Map = (props) => {
  const { navigation } = props;
  return (
    <MapView
      style={styles.map}
      initialRegion={{
        latitude: props.latitude,
        longitude: props.longitude,
        latitudeDelta: 0.009,
        longitudeDelta: 0.009,
      }}
    >
      <Marker
        coordinate={{ latitude: props.latitude, longitude: props.longitude }}
      >
        <Image
          source={{ uri: props.imageURI }}
          style={{
            height: 55,
            width: 55,
            borderWidth: 3,
            borderColor: "#003f5c",
            borderRadius: 20,
            alignSelf: "center",
            marginVertical: 20,
          }}
        />
      </Marker>

      {props.photosObjectAlbum.map((marker, index) => (
        <Marker
          key={index}
          coordinate={{
            latitude: marker.coords.latitude,
            longitude: marker.coords.longitude,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.nav.navigate("SpecificPhoto", {
                imageObj: marker,
                photosObjectAlbum: Object.values(props.photosObjectAlbum),
              });
            }}
          >
            <Image
              source={{ uri: marker.url }}
              style={{
                height: 45,
                width: 45,
                borderWidth: 3,
                borderColor: "white",
                borderRadius: 20,
                alignSelf: "center",
                marginVertical: 20,
              }}
            />
          </TouchableOpacity>
        </Marker>
      ))}
    </MapView>
  );
};

const styles = StyleSheet.create({
  map: {
    height: 350,
  },
});

export default Map;
