import React from "react";
import { View, Image, StyleSheet } from "react-native";

const ResultsDetailAlbum = (props) => {
  return (
    <View style={styles.container}>
      <Image style={styles.imageStyle} source={{ uri: props.photo.url }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 15,
  },
  imageStyle: {
    width: 250,
    height: 200,
    borderRadius: 4,
    marginBottom: 5,
  },
});

export default ResultsDetailAlbum;
