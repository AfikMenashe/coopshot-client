/** @format */

import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { FAB, Portal, Provider } from "react-native-paper";
import {
  Feather,
  Ionicons,
  MaterialIcons,
  FontAwesome5,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

const FloatingButtonComp = (props) => {
  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  //------- Floating Button --------
  const [state, setState] = React.useState({ open: false });

  const onStateChange = ({ open }) => setState({ open });

  const { open } = state;

  return (
    <Provider style={styles.providerStyle}>
      <Portal>
        <FAB.Group
          color="black"
          open={open}
          icon={open ? "minus" : "plus"}
          actions={[
            {
              icon: () => (
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <MaterialIcons name="photo-library" size={24} color="black" />
                </View>
              ),

              label: "Gallery",
              onPress: () => {
                props.nav.navigate("MultiPicker", {
                  albumId: props.albumId,
                  maxSelections: 10,
                });
              },
              small: false,
            },
            {
              icon: (props) => (
                <Ionicons {...props} name="camera" size={24} color="black" />
              ),
              label: "Camera",
              onPress: () => {
                props.nav.navigate("Camera", {
                  albumId: props.albumId,
                  albumName: props.albumName,
                }); //Need to pass here the album id and the FormDataFunc
              },
              small: false,
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {
            if (open) {
              // do something if the speed dial is open
            }
          }}
        />
      </Portal>
    </Provider>
  );
};

const styles = StyleSheet.create({
  providerStyle: {
    position: "absolute",
    bottom: 10,
    right: 10,
    marginBottom: 10,
    marginRight: 10,
  },
});

export default FloatingButtonComp;
