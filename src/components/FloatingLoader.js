import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";

const FloatingLoader = () => (
  <View style={[styles.container, styles.horizontal]}>
    <Text style={{ color: "white" }}></Text>
    <ActivityIndicator size="large" color="#00ff00" />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    position: "absolute",

  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    position: "absolute",
    marginTop: 75,
    padding: 10,
    alignSelf: "center"

  }
});

export default FloatingLoader;