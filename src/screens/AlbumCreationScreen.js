/** @format */

import React, { useState, useContext } from "react";
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
} from "react-native";

import { FAB } from "react-native-paper";

import { Context as AlbumContext } from "../context/AlbumContext";
// import { Context as AuthContext } from "../context/AuthContext";
import axios from "../api/albums";

const AlbumCreationScreen = ({ navigation }) => {
  const { createNewAlbum } = useContext(AlbumContext);
  // const AuthContextUse = useContext(AuthContext);
  const [albumName, setAlbumName] = useState("");
  const [description, setDescription] = useState("");

  return (
    <ImageBackground
      style={{ height: "100%", width: "100%" }}
      source={require("../../assets/back3_1.png")}
    >
      {/* <View> */}
      <View style={styles.viewDiv}>
        <Text style={styles.text}>Create New Album</Text>
        <TextInput
          style={styles.InputStyle}
          placeholder={"Enter Album Name"}
          value={albumName}
          onChangeText={(text) => setAlbumName(text)}
        />
        <TextInput
          style={styles.InputStyle}
          placeholder={"Enter Description"}
          textAlignVertical={"top"}
          numberOfLines={10}
          value={description}
          onChangeText={(text) => setDescription(text)}
        />
        <TouchableOpacity
          style={styles.btCreate}
          onPress={() => {
            createNewAlbum(albumName, description, (_id) => {
              navigation.replace("MultiPicker", {
                albumId: _id,
                maxSelections: 10,
                path: "createNewAlbum",
              });
            });
          }}
        >
          <Text style={styles.textBtn}>Create New Album</Text>
        </TouchableOpacity>
      </View>
      {/* </View> */}
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    paddingLeft: 20,
    margin: 25,
  },
  InputStyle: {
    fontSize: 18,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 20,
    padding: 15,
    margin: 15,
    borderRadius: 10,
  },
  label: {
    fontSize: 20,
    marginBottom: 5,
  },
  btCreate: {
    alignItems: "center",
    backgroundColor: "#fb5b5a",
    justifyContent: "flex-end",
    padding: 10,
    borderRadius: 50,
    margin: 10,
    borderWidth: 4,
  },
  textBtn: {
    textTransform: "uppercase",
    fontSize: 15,
    padding: 3,
  },

  viewDiv: {
    marginHorizontal: 20,
    paddingVertical: 120,
  },
  fab: {
    position: "absolute",
    bottom: 0,
    right: 10,
    // marginBottom: 10,
    marginRight: 10,
  },
});

export default AlbumCreationScreen;
