/** @format */

import React, { useContext, useState, useEffect } from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  FlatList,
  Image,
  SafeAreaView,
} from "react-native";
import { Context as AlbumContext } from "../context/AlbumContext";
import { Context as AuthContext } from "../context/AuthContext";
import { Feather, AntDesign } from "@expo/vector-icons";
import Loader from "../components/Loader";
import FloatingButtonComp from "../components/FloatinButtonComp";
import MasonryList from "react-native-masonry-list";
import Constants from "expo-constants";
import { withNavigationFocus } from "react-navigation";
import { LinearGradient } from "expo-linear-gradient";

const AlbumSingleScreen = (props) => {
  const { navigation } = props;
  const [albumIndex, setAlbumIndex] = useState(null);
  const { state, getAlbum, editAlbumName } = useContext(AlbumContext);
  const [album, setAlbum] = useState(null);
  const authCont = useContext(AuthContext);
  const authState = authCont.state;
  const albumID = navigation.getParam("albumId");
  async function useEffectWithAsync() {
    for (let i = 0; i < state.length; i++) {
      if (state[i]._id === albumID) {
        setAlbumIndex(i);
        setAlbum(state[i]);
        break;
      }
    }
  }

  useEffect(() => {
    // Call async action with focusing screen
    useEffectWithAsync();
  }, [state[albumIndex]]);

  if (!album)
    return (
      <View>
        <Loader />
      </View>
    );

  return (
    <View style={styles.mainContainerStyle}>
      <View
        style={{
          backgroundColor: "#003f5c",
          height: 170,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          paddingHorizontal: 20,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            alignItems: "center",
            position: "absolute",
            right: 10,
            top: 30,
          }}
        ></View>
        <Image
          //   source={require("../images/1.png")}
          style={{
            height: 10,
            width: 20,
            marginTop: 50,
          }}
        />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            width: "100%",
          }}
        >
          <View style={{ width: "90%" }}>
            <Text
              style={{
                fontSize: 28,
                color: "#FFF",
                fontWeight: "bold",
              }}
            >
              {album.name}
            </Text>
          </View>

          <View
            style={{
              width: "10%",
            }}
          >
            {/* //Edit Icon - AlbumName*/}
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("EditAlbumName", { id: album._id })
              }
            >
              <Feather name="edit" size={18} color="black" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <LinearGradient
        colors={["rgba(0,63,92,0.4)", "transparent"]}
        style={{
          left: 0,
          right: 0,
          marginTop: -65,
        }}
      >
        <View
          style={{
            backgroundColor: "#FFF",
            paddingVertical: 8,
            paddingHorizontal: 20,
            marginHorizontal: 20,
            borderRadius: 15,
            marginTop: 25,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: "95%",
            }}
          >
            <Text>{album.description}</Text>
          </View>
          <TouchableOpacity
            style={{
              width: "10%",
            }}
            onPress={() =>
              navigation.navigate("EditAlbumDesc", { id: album._id })
            }
          >
            <Feather name="edit" size={18} color="black" />
          </TouchableOpacity>
        </View>
      </LinearGradient>

      <View // album partners
        style={styles.photosContainerStyle}
      >
        <FlatList
          data={album.profileImages}
          horizontal
          keyExtractor={(photo) => photo._id}
          renderItem={({ item }) => {
            return (
              <View>
                <Image
                  source={{
                    uri: item.uri,
                  }}
                  style={styles.photo}
                />
              </View>
            );
          }}
        />
        {/* //Edit Icon - DescAlbum*/}
        <View style={{ justifyContent: "flex-end", marginTop: 10 }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("AddPartnerToAlbum", {
                id: album._id,
              });
            }}
          >
            <AntDesign name="adduser" size={30} color="black" />
          </TouchableOpacity>
        </View>
      </View>

      {/* component for presenting all photos */}
      <View style={styles.masonryContainerStyle}>
        <MasonryList
          onPressImage={(data, index) => {
            navigation.navigate("SpecificPhoto", {
              imageObj: data,
              photosObjectAlbum: Object.values(album.photos),
            });
          }}
          columns={3}
          images={Object.values(album.photos)}
          imageContainerStyle={{
            borderRadius: 7,
            height: 100,
          }}
        />
      </View>

      <FloatingButtonComp
        nav={navigation}
        albumName={album.name}
        albumId={album._id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  nameStyle: {
    fontWeight: "bold",
    fontSize: 36,
    color: "white",
  },

  textContainerStyle: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    height: "auto",
    marginBottom: 5,
    marginLeft: 10,
  },
  photosContainerStyle: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    height: "auto",
    // marginBottom: 5,
  },
  masonryContainerStyle: {
    paddingTop: 10,
    height: "89%",
  },
  mainContainerStyle: {
    flex: 1,
  },
  photo: {
    height: 30,
    width: 30,
    borderRadius: 20,
    alignSelf: "center",
    marginHorizontal: 5,
  },
});

export default withNavigationFocus(AlbumSingleScreen);
