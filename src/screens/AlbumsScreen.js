import React, { useContext, useEffect, useState, useRef } from "react";
import {
  Text,
  StyleSheet,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  AppState,
  AsyncStorage,
  SafeAreaView,
} from "react-native";
import axios from "../api/albums";
import { AntDesign } from "@expo/vector-icons";
import { Context as AlbumContext } from "../context/AlbumContext";
import { Context as GlobalContext } from "../context/GlobalContext";
import { Context as AuthContext } from "../context/AuthContext";
import { MaterialIcons, Feather } from "@expo/vector-icons";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";
import { Picker } from "@react-native-picker/picker";
import { SinglePickerMaterialDialog } from "react-native-material-dialog";
import { withNavigationFocus } from "react-navigation";
import RNPickerSelect from "react-native-picker-select";
import shorthash from "shorthash";
import * as FileSystem from "expo-file-system";
import FloatingLoader from "../components/FloatingLoader";
import { DotIndicator } from "react-native-indicators";
import { render } from "react-dom";

const colorsArr = ["#ffa06c", "#bb32fe", "#ff5c83", "#5facdb"];

const AlbumsScreen = (props) => {
  const { navigation } = props;
  const { editDefaultAlbum, getDefaultAlbum, updateFavAlbums } =
    useContext(GlobalContext);
  const [didLoad, setDidLoad] = useState(false);
  const [singlePickerVisible, setSinglePickerVisible] = useState(false);
  const [singlePickerSelectedItem, setSinglePickerSelectedItem] =
    useState("Default Album");
  const { state, getAlbums, deleteAlbum, setNewState } =
    useContext(AlbumContext);
  const stateGlobal = useContext(GlobalContext);
  const stateAuth = useContext(AuthContext);
  const [valuePicker, setValuePicker] = useState(""); //Dont remove the valuePicker -> Importent !!!!
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const [loaderToggle, setloaderToggle] = useState(true);
  const [newData, setNewData] = useState(state.slice());
  var userIdLocalStorage = "";
  const [filterData, setFilterData] = useState(state.slice());

  const onItemSelected = async (result) => {
    let arraySplit = [];
    if (result.selectedItem != null) {
      arraySplit = result.selectedItem.value.split(",");
    }
    editDefaultAlbum(arraySplit[0], arraySplit[1]);
    setSinglePickerVisible(false);
    const curID = await AsyncStorage.getItem("retUserId");
    const fullKey = curID + " default album";
    await AsyncStorage.setItem(fullKey, arraySplit[1]);
    setSinglePickerSelectedItem(arraySplit[1]);
  };

  const useEffectAsync = async () => {
    userIdLocalStorage = await AsyncStorage.getItem("retUserId");
    let defaultAlbum = await AsyncStorage.getItem(
      userIdLocalStorage + " default album"
    );
    if (defaultAlbum) setSinglePickerSelectedItem(defaultAlbum);
    else setSinglePickerSelectedItem("Default Album");
    // Make the request from the server
    const response = await axios.get("/album/all").then((albums) => {
      const albums2 = albums.data.map((item) => {
        return {
          _id: item._id,
          name: item.name.split("~")[1],
          photos: item.photos,
          description: item.description,
          photosQuantity: item.photosQuantity,
        };
      });
      return albums2;
    });

    //Create new state with hashed images

    //Method 1
    var hashedPhotosArr = [];
    for (let i = 0; i < response.length; i++) {
      for (let j = 0; j < response[i].photos.length; j++) {
        const cachedPhoto = await checkImageInCache(response[i].photos[j]);
        hashedPhotosArr.push(cachedPhoto);
      }
      response[i].photos = hashedPhotosArr;
      hashedPhotosArr = [];
    }

    setNewState(response); //Set the new state in Context
    setNewData(response); //Set state locally
    setloaderToggle(!loaderToggle);
    await AsyncStorage.setItem(
      userIdLocalStorage + " Albums State Coopshoot",
      JSON.stringify(response)
    );
    return;
  };

  // useEffect(() => {
  //   if (props.isFocused === true) {
  //     if (!didLoad) {
  //       getDefaultAlbum();
  //       useEffectAsync();
  //       setDidLoad(true);
  //     }
  //   }
  // }, [...state]);

  useEffect(() => {
    useEffectAsync();
  }, [...state]);

  useEffect(() => {
    getDefaultAlbum();
  }, [stateGlobal.defaultAlbumID]);

  function searchAlbum(textToSearch) {
    setNewData(
      state.filter((i) =>
        i.name.toLowerCase().startsWith(textToSearch.toLowerCase())
      )
    );
  }
  const checkImageInCache = async (element) => {
    const uri = element.url;
    const name = shorthash.unique(uri);
    const path = `${FileSystem.cacheDirectory}${name}`;
    const image = await FileSystem.getInfoAsync(path);
    const resultImage = element;
    if (image.exists) {
      resultImage.url = image.uri;
    } else {
      console.log("downloading image to cache");
      const newImage = await FileSystem.downloadAsync(uri, path);
      resultImage.url = newImage.uri;
    }
    return resultImage;
  };

  //Albums Names for setting default album

  const arrAlbumsNames = [];
  state.map((album) => {
    arrAlbumsNames.push({
      label: album.name,
      value: album._id + "," + album.name,
    });
  });

  return (
    <SafeAreaView style={{ flex: 1, height: 400 }}>
      <View
        style={{
          backgroundColor: "#FFF",
          flex: 1,
        }}
      >
        <View
          style={{
            backgroundColor: "#003f5c",
            height: 170,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            paddingHorizontal: 20,
          }}
        >
          {/* {loaderToggle && <FloatingLoader animating={true} />} */}
          {/* {loaderToggle && <DotIndicator animating={true} hidesWhenStopped={true}/>} */}
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              position: "absolute",
              right: 10,
              top: 30,
            }}
          >
            <TouchableOpacity onPress={() => setSinglePickerVisible(true)}>
              <View
                style={{
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("../../assets/crown.png")}
                  style={{ height: 20, width: 20 }}
                />
                <Text style={{ color: "white", fontWeight: "700" }}>
                  {singlePickerSelectedItem}
                </Text>
              </View>
            </TouchableOpacity>

            <SinglePickerMaterialDialog
              title={"Pick one Album!"}
              scrolled
              items={state.map((album, index) => ({
                value: album._id + "," + album.name,
                label: album.name,
              }))}
              visible={singlePickerVisible}
              selectedItems={singlePickerSelectedItem}
              onCancel={() => setSinglePickerVisible(false)}
              onOk={(result) => onItemSelected(result)}
            />
          </View>
          <Image
            //   source={require("../images/1.png")}
            style={{
              height: 10,
              width: 20,
              marginTop: 50,
            }}
          />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 25,
              width: "100%",
            }}
          >
            <View style={{ width: "60%" }}>
              <Text
                style={{
                  fontSize: 28,
                  color: "#FFF",
                  fontWeight: "bold",
                }}
              >
                My Albums
              </Text>
            </View>

            <View
              style={{
                width: "40%",
              }}
            ></View>
          </View>
        </View>
        <LinearGradient
          colors={["rgba(0,63,92,0.4)", "transparent"]}
          style={{
            left: 0,
            right: 0,
            height: 90,
            marginTop: -45,
          }}
        >
          <View
            style={{
              backgroundColor: "#FFF",
              paddingVertical: 8,
              paddingHorizontal: 20,
              marginHorizontal: 20,
              borderRadius: 15,
              marginTop: 25,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <AntDesign name="search1" size={18} color="black" />
            <TextInput
              placeholder="Search"
              onChangeText={(text) => {
                searchAlbum(text);
              }}
              placeholderTextColor="#003f5c"
              style={{
                fontWeight: "bold",
                fontSize: 18,
                width: 260,
                marginLeft: 10,
              }}
            />
            <Image
              // source={require("../images/3.png")}
              style={{ height: 20, width: 20 }}
            />
          </View>
        </LinearGradient>

        <View
          style={{
            flexDirection: "row",
            paddingHorizontal: 20,
            width: "100%",
            alignItems: "center",
          }}
        >
          <View style={{ width: "50%" }}>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 17,
                color: "#585a61",
              }}
            >
              All Albums
            </Text>
            <View
              style={{
                height: 4,
                backgroundColor: "#003f5c",
                width: 115,
              }}
            ></View>
          </View>
        </View>
        <SafeAreaView style={styles.container}>
          <FlatList
            style={styles.list}
            contentContainerStyle={styles.listContainer}
            data={newData}
            horizontal={false}
            numColumns={2}
            keyExtractor={(album) => album._id}
            ItemSeparatorComponent={() => {
              return <View style={styles.separator} />;
            }}
            renderItem={({ item }) => {
              return (
                <View style={styles.card}>
                  <View style={styles.imageContainer}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("SingleAlbum", {
                          albumId: item._id,
                        })
                      }
                    >
                      {item.photos != null ? (
                        item.photos[0] != null ? (
                          <Image
                            style={styles.cardImage}
                            source={{ uri: item.photos[0].url }}
                          />
                        ) : (
                          <Image
                            style={styles.cardImage}
                            source={require("../../assets/No_Image_Available.jpg")}
                          />
                        )
                      ) : null}
                    </TouchableOpacity>
                    <View style={styles.cardContent}>
                      <View style={{ display: "flex", flexDirection: "row" }}>
                        <Text
                          style={{
                            fontWeight: "bold",
                            width: "80%",
                          }}
                        >
                          {item.name}
                        </Text>
                        <TouchableOpacity
                          onPress={() => updateFavAlbums(item._id)}
                        >
                          {stateGlobal.state.favAlbums.find(
                            (album) => album._id === item._id
                          ) ? (
                            <AntDesign name="staro" size={24} color="gold" />
                          ) : (
                            <AntDesign name="staro" size={24} color="black" />
                          )}
                        </TouchableOpacity>
                      </View>

                      <Text
                        style={{
                          paddingHorizontal: 10,
                          fontWeight: "bold",
                          color: "#003f5c",
                          paddingTop: 3,
                        }}
                      >
                        ({item.photos != null ? item.photosQuantity : null})
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
          />
        </SafeAreaView>
      </View>
    </SafeAreaView>
  );
};

//-- Side Buttons --
AlbumsScreen.navigationOptions = ({ navigation }) => {
  return {
    headerRight: () => (
      <TouchableOpacity onPress={() => navigation.navigate("AlbumCreation")}>
        <AntDesign
          style={{ marginRight: 15 }}
          name="plus"
          size={24}
          color="black"
        />
      </TouchableOpacity>
    ),
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  list: {
    paddingHorizontal: 10,
  },
  listContainer: {
    alignItems: "center",
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/
  card: {
    marginVertical: 8,
    backgroundColor: "white",
    flexBasis: "45%",
    marginHorizontal: 10,
  },
  cardContent: {
    paddingVertical: 17,
    justifyContent: "space-between",
  },
  cardImage: {
    flex: 1,
    height: 150,
    width: null,
  },
  imageContainer: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9,
  },
  /******** card components **************/
  title: {
    fontSize: 18,
    flex: 1,
    color: "#778899",
  },
  count: {
    fontSize: 18,
    flex: 1,
    color: "#B0C4DE",
  },
  plusButton: {
    position: "absolute",
    bottom: 10,
    right: 10,
    marginBottom: 10,
    marginRight: 10,
  },
});

export default withNavigationFocus(AlbumsScreen);
