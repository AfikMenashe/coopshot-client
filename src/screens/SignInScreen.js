import React, { useContext,useEffect } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  AsyncStorage
} from 'react-native';
import { color } from 'react-native-reanimated';
import { NavigationEvents } from 'react-navigation';
import AuthForm from '../components/AuthForm';
import { Context } from '../context/AuthContext';
import NavLink from '../components/NavLink';

const SignInScreen = () => {

  const { state, signin, clearErrorMessage } = useContext(Context);
  // await AsyncStorage.removeItem('data');
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#003f5c" />
      <NavigationEvents onWillFocus={clearErrorMessage} />
      <AuthForm
        headerText="CoopShoot"
        style={styles.logo}
        errorMessage={state.errorMessage}
        onSubmit={signin}
        submitButtonText="Login"
      />
      <TouchableOpacity>
        <NavLink
          style={{ color: 'white', textDecoration: 'none' }}
          text="Sign Up"
          routeName="SignUp"
        />
      </TouchableOpacity>
    </View>
  );
};

SignInScreen.navigationOptions = {
  header: () => false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
    //marginBottom: 250,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#fb5b5a',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 80,
    marginBottom: 10,
  },
  loginText: {
    color: '#FFFFFF',
  },
});

export default SignInScreen;
