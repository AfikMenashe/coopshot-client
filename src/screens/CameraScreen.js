import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Switch,
  Alert,
} from "react-native";
import { Camera } from "expo-camera";
import { MaterialIcons, Ionicons, AntDesign } from "@expo/vector-icons";
import { Context } from "../context/GlobalContext";

const CameraScreen = ({ navigation }) => {
  const { state, getDefaultAlbum } = useContext(Context);

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const [hasPermission, setHasPermission] = useState(null);
  const [cameraType, setCameraType] = useState("back");
  const [flashMode, setFlashMode] = useState("off");
  const [flashIcon, setFlashIcon] = useState("flash-off");
  var albumName = navigation.getParam("albumName");
  var albumId = navigation.getParam("albumId");
  const cameraRef = React.createRef(); // create ref for Camera component

  if (albumId == undefined || albumName == undefined) {
    albumId = state.defaultAlbumID;
    albumName = state.defaultAlbumName;
    // if user didn't choose defult album
    if (!albumId || !albumName) {
      albumId = navigation.getParam("albumId");
      if (!albumId) {
        // verify if the user navigate from SingleAlbum screen
        Alert.alert("No default album choosen");
        navigation.goBack();
      }
    }
  }

  useEffect(() => {
    getDefaultAlbum();
    // ask for permission first time the screen is focused
    if (albumId && albumName)
      (async () => {
        const { status } = await Camera.requestPermissionsAsync();
        setHasPermission(status === "granted");
      })();
  }, []);

  if (hasPermission === null) {
    // check permission
    return <View />;
  }
  if (hasPermission === false) {
    // check permission
    return <Text>No access to camera</Text>;
  }

  const takePicture = async () => {
    // function for taking a picture , onPress camera button
    try {
      if (cameraRef.current) {
        const options = { quality: 0.5, base64: true };
        var photo = await cameraRef.current.takePictureAsync(); // async function wiil return photo  elemnt with(uri.width,height)
        navigation.navigate("ToUpload", {
          photo: photo,
          albumId,
          isEnabled,
        });
      }
    } catch (err) {
      console.log("There is an error with the camera Screen " + err);
    }
  };

  const handleFlashMode = () => {
    if (flashMode === "on") {
      setFlashMode("auto");
      setFlashIcon("flash-auto");
    } else if (flashMode === "off") {
      setFlashMode("on");
      setFlashIcon("flash-on");
    } else {
      setFlashMode("off");
      setFlashIcon("flash-off");
    }
  };

  const switchCamera = () => {
    if (cameraType === "back") {
      setCameraType("front");
    } else {
      setCameraType("back");
    }
  };

  return (
    <>
      <View style={{ flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          ref={cameraRef}
          type={cameraType}
          flashMode={flashMode}
        >
          <View style={styles.ToggleContainer}>
            <Ionicons name="ios-people-outline" size={32} color="white" />
            <Switch
              style={{ marginLeft: 15 }}
              trackColor={{ false: "#767577", true: "#DF7401" }}
              thumbColor={isEnabled ? "#f4f3f4" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
            <AntDesign
              name="user"
              size={24}
              color="white"
              style={{ marginLeft: 15, marginTop: 5 }}
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity // touch button for fliping camera
              style={styles.button}
              onPress={switchCamera}
            >
              <Ionicons name="camera-reverse-sharp" style={styles.button} />
            </TouchableOpacity>
            <TouchableOpacity // touch button for fliping camera
              style={styles.button}
              onPress={takePicture}
            >
              <View style={styles.cameraBtnRadius}>
                <View style={styles.cameraBtn}></View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity // touch button for turn on/off flash
              style={styles.button}
              onPress={handleFlashMode}
            >
              <MaterialIcons name={flashIcon} style={styles.button} />
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: 'flex-end',
    margin: 20,
    //alignContent: "space-between",
    justifyContent: "space-between",
    marginBottom: 30,
  },
  button: {
    alignItems: "center",
    fontSize: 35,
    alignSelf: "flex-end",

    color: "white",
  },
  text: {
    fontSize: 18,
    color: "white",
  },
  cameraBtn: {
    borderWidth: 2,
    borderRadius: 50,
    borderColor: "white",
    height: 50,
    width: 50,
    backgroundColor: "white",
    flex: 0.8,
  },
  cameraBtnRadius: {
    borderWidth: 2,
    borderRadius: 50,
    borderColor: "white",
    height: 65,
    width: 65,
    //display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  ToggleContainer: {
    flex: 1,
    flexDirection: "row",
    marginTop: 60,
    justifyContent: "center",
    backgroundColor: "transparent",
    alignSelf: "auto",
  },
});

export default CameraScreen;
