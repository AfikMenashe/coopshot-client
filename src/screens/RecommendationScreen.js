import { Tab, Tabs, ScrollableTab, Button } from "native-base";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Loader from "../components/Loader";
import {
  getCurrentPositionAsync,
  requestPermissionsAsync,
  hasServicesEnabledAsync,
} from "expo-location";
import axios from "../api/albums";
import TabsComp from "../components/TabsComp";

const w = Dimensions.get("window").width;
const h = Dimensions.get("window").height;

const RecommendationScreen = ({ navigation }) => {
  const [tagsRecommendedList, setTagsRecommendedList] = useState(null);
  const [tagObjects, setTagObjects] = useState([]);
  const [tagsPhotosRecommendedArray, setTagsPhotosRecommendedArray] =
    useState(null);

  const getLocation = async () => {
    try {
      let { status } = await requestPermissionsAsync();
      if (status !== "granted") {
        console.error("Permission to access location was denied");
        return;
      }

      if (!hasServicesEnabledAsync()) {
        console.error("Locatin services must be enabled");
        return;
      }

      let glocation = await getCurrentPositionAsync({});

      const longUser = glocation.coords.longitude;
      const latUser = glocation.coords.latitude;
      return { longUser, latUser };
    } catch (err) {
      console.error(err);
    }
  };

  const getRecommendation = async () => {
    try {
      const { longUser, latUser } = await getLocation();
      console.log(
        "Sending Location of currennt user to The Algorithem Server , Long: " +
        longUser +
        " lat: " +
        latUser
      );
      const tags = await axios.get("/api/recommendation", {
        params: {
          latUser: latUser,
          longUser: longUser,
        },
      });

      setTagObjects(tags.data.tagObjectsList);
      setTagsRecommendedList(tags.data.tagsList);
      setTagsPhotosRecommendedArray(tags.data.tagPhotos);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getRecommendation();
  }, []);

  return (
    <ScrollView
      style={{
        backgroundColor: "#FFF",
      }}
    >
      <View
        style={{
          height: 0.5 * h,
          backgroundColor: "#003f5c",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: 20,
            marginTop: 60,
            alignItems: "center",
          }}
        >
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require("../../assets/recommend/icons-back-light.png")}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            alignSelf: "center",
            alignItems: "center",
            marginTop: 0.1 * h,
          }}
        >
          <Ionicons name="ios-pricetags-outline" size={104} color="white" />
          <Text
            style={{
              fontSize: 26,
              color: "#FFF",
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Recommendation
          </Text>
          {tagObjects != null && tagsPhotosRecommendedArray != null ? (
            <View>
              <TabsComp
                tagObjectsList={tagObjects}
                tagsPhotosRecommendedArray={tagsPhotosRecommendedArray}
              />
            </View>
          ) : (
            <Loader />
          )}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({});

export default RecommendationScreen;
