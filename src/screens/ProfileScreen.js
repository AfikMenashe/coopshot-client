import React, { useEffect, useState, useContext } from 'react';
import {
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from '@expo/vector-icons/Entypo';
import { Context as AuthContext } from '../context/AuthContext';
import Loader from '../components/Loader';
import { AsyncStorage } from 'react-native';
import axios, { UploadProfilePic } from '../api/albums';
import { Feather, Entypo, FontAwesome5 } from '@expo/vector-icons';

const ProfileScreen = ({ navigation }) => {
  const { signout } = useContext(AuthContext);
  const [editPage, setEditPage] = useState(false);
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [name, setName] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const onBack = (data) => {
    setImageUrl(data);
  };

  const submitEdit = () => {
    if (editPage) {
      UploadProfilePic(name, email, phone, imageUrl);
    }
    setEditPage(!editPage);
  };
  useEffect(() => {
    const getUserInfo = async () => {
      const currentUser = await AsyncStorage.getItem('retUserId');
      // const author = await axios.get('/user/finduserbyid', {
      //   userId: currentUser,
      // });
      // setUserInfo(author.data);
      const author = JSON.parse(await AsyncStorage.getItem(currentUser + " user details"))
      setEmail(author.email);
      setPhone(author.phone);
      setName(author.name);
      setImageUrl({ uri: author.image });
    };
    getUserInfo();
  }, []);

  
  return (
    <View
      style={{
        backgroundColor: '#003f5c',
        height: '100%',
      }}
    >
      <View
        style={{
          paddingHorizontal: 40,
          backgroundColor: '#FFF',
          height: '40%',
          borderBottomLeftRadius: 50,
          borderBottomRightRadius: 50,
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            marginTop: 40,
            justifyContent: 'space-between',
          }}
        >
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="chevron-left" size={24} color="#044244" />
          </TouchableOpacity>
          <View
            style={{
              alignItems: 'flex-end',
            }}
          >
            <TouchableOpacity onPress={() => submitEdit()}>
              {editPage ? (
                <FontAwesome5 name="save" size={24} color="black" />
              ) : (
                <FontAwesome5 name="pencil-alt" size={24} color="#044244" />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          disabled={!editPage}
          onPress={() => {
            navigation.navigate('MultiPicker', { onBack, maxSelections: 1 });
          }}
        >
          <Image
            source={{
              uri: imageUrl.uri,
            }}
            style={{
              height: 100,
              width: 100,
              borderRadius: 20,
              alignSelf: 'center',
              marginVertical: 20,
            }}
          />
        </TouchableOpacity>

        {editPage ? (
          <TextInput
            style={{
              fontSize: 22,
              color: '#044244',
              alignSelf: 'center',
              borderBottomColor: '#000000',
              borderBottomWidth: 1,
            }}
            placeholder="Name"
            placeholderTextColor="#044244"
            value={name}
            onChangeText={setName}
            autoCapitalize="none"
            autoCorrect={false}
          />
        ) : (
          <Text
            style={{
              fontSize: 22,
              color: '#044244',
              alignSelf: 'center',
            }}
          >
            {name}
          </Text>
        )}
      </View>

      <View
        style={{
          height: '60%',
          justifyContent: 'space-between',
        }}
      >
        <View style={{ alignItems: 'flex-start' }}>
          <View
            style={{
              backgroundColor: '#728c8e',
              width: 330,
              marginHorizontal: 40,
              borderRadius: 20,
              marginTop: 30,
            }}
          >
            <View
              style={{
                paddingVertical: 20,
                paddingHorizontal: 30,
                flexDirection: 'row',
              }}
            >
              <Feather name="phone" size={24} color="white" />
              {editPage ? (
                <TextInput
                  style={{
                    marginLeft: 15,
                    marginTop: 3,
                    color: '#FFF',
                    fontSize: 15,
                    width: '100%',
                  }}
                  placeholder="Phone"
                  placeholderTextColor="#003f5c"
                  value={phone}
                  onChangeText={setPhone}
                  autoCapitalize="none"
                  autoCorrect={false}
                />
              ) : (
                <Text
                  style={{
                    marginLeft: 15,
                    marginTop: 3,
                    color: '#FFF',
                    fontSize: 15,
                  }}
                >
                  {phone}
                </Text>
              )}
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#728c8e',
              width: 330,
              marginHorizontal: 40,
              borderRadius: 20,
              marginTop: 30,
            }}
          >
            <View
              style={{
                paddingVertical: 20,
                paddingHorizontal: 30,
                flexDirection: 'row',
              }}
            >
              <Entypo name="email" size={24} color="white" />

              {editPage ? (
                <TextInput
                  style={{
                    marginLeft: 15,
                    marginTop: 3,
                    color: '#FFF',
                    fontSize: 15,
                    width: '100%',
                  }}
                  placeholder="Email"
                  placeholderTextColor="#003f5c"
                  value={email}
                  onChangeText={setEmail}
                  autoCapitalize="none"
                  autoCorrect={false}
                />
              ) : (
                <Text
                  style={{
                    marginLeft: 15,
                    marginTop: 3,
                    color: '#FFF',
                    fontSize: 15,
                  }}
                >
                  {email}
                </Text>
              )}
            </View>
          </View>
        </View>

        <View
          style={{
            alignItems: 'flex-end',
          }}
        >
          <TouchableOpacity
            onPress={() => signout()}
            style={{
              marginHorizontal: 40,
              width: '80%',
              backgroundColor: '#fb5b5a',
              borderRadius: 25,
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 35,
            }}
          >
            <Text style={{ fontSize: 20, color: '#FFFFFF' }}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

// const styles = StyleSheet.create({
//   image: {
//     alignItems: 'center',
//     width: 400,
//     height: 600,
//   },
//   row: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     alignItems: 'flex-start',
//     marginTop: 20,
//   },
// });

export default ProfileScreen;
